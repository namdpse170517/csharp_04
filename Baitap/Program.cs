﻿// Tạo mảng thứ nhất
int[] arr1 = { 1, 2, 3 };

// Tạo mảng thứ hai
int[] arr2 = { 1, 2, 3 };

Console.WriteLine("arr1 = arr2 " + ReferenceEquals(arr1, arr2));

// Gán mảng 3 bằng mảng 2
int[] arr3 = arr2;

Console.WriteLine("arr3 = arr2 " + ReferenceEquals(arr3, arr2));